ScrewTurnWiki
=============

Fork of https://stw.codeplex.com/ 

(Based on version *3.0.5.600*)

**Version 3.0.7.800 ** 

* Upgraded to Visual Studio 2015
* All projects target .NET Framework 4.6.1
* Roslyn compiler (C# 6.0) included into the packages during build
* Replaced multiple occurences of String.Format by string interpolation
* Using *NameOf* instead of string argument names wherever possible
* Modified build batches and web.config files
* Upgraded NUnit from 2.5.9.10348 to 2.6.4.14350
* Upgraded Rhino.Mocks from 3.5.0.0 to 3.6.0.0
* Added NUnit Test adapter 2.0.0 package to allow running tests inside Visual Studio
* Minor fix in FormatterTests.Setup()
