<div style="float: right;">
<a href="{NAMESPACE}.RSS.aspx" title="Update notifications for {WIKITITLE} ({NAMESPACE}) (RSS 2.0)"><img src="{THEMEPATH}Images/RSS.png" alt="RSS" /></a>
<a href="{NAMESPACE}.RSS.aspx?Discuss=1" title="Update notifications for {WIKITITLE} Discussions ({NAMESPACE}) (RSS 2.0)"><img src="{THEMEPATH}Images/RSS-Discussion.png" alt="RSS" /></a></div>
====Navigation ({NAMESPACE})====
* '''[MainPage|Main Page]'''
* [++MainPage|Main Page (root)]

* [RandPage.aspx|Random Page]
* [Edit.aspx|Create a new Page]
* [AllPages.aspx|All Pages]
* [Category.aspx|Categories]
* [NavPath.aspx|Navigation Paths]

* [AdminHome.aspx|Administration]
* [Upload.aspx|File Management]

* [Register.aspx|Create Account]

<small>'''Search the wiki'''</small>{BR}
{SEARCHBOX}

[image|PoweredBy|Images/PoweredBy.png|http://www.screwturn.eu]